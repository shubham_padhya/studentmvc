/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package mvcpattern;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class MVCPattern {

    
    public static void main(String[] args) {
        Student model = retriveStudentFromDatabase();
        
        StudentView view = new StudentView();
        StudentController controller = new StudentController(model,view);
        
        controller.updateView();
        
        controller.setStudentName("shubham");
        controller.updateView();
    }
    private static Student retriveStudentFromDatabase(){
        
        Student student = new Student();
        student.setName("dhruv");
        student.setRollNo("54");
        return student;
    }

    }
 
    
    
    

