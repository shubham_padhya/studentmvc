/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mvcpattern;

/**
 *
 * @author SHUBHAM PADHYA
 */
public class Student {
        private String roolNo;
    private String name;
    
    public String getRollNo(){
        return roolNo;
    }
    public void setRollNo(String roolNo){
        this.roolNo = roolNo;
    }
    
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

}
